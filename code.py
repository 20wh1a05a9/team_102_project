print("enter the number of producer and consumer companies:")
m, n = map(int, input().split())

# giving an input of price and date for all producer companies
producer = []
for i in range(m):
    print("enter the price and date of producer company:", i + 1)
    price, date = map(int, input().split())
    p = [price, date]
    producer.append(p)

# giving an input of price and date for all consumer companies
consumer = []
for i in range(n):
    print("enter the price and date of consumer company:", i + 1)
    price, date = map(int, input().split())
    c = [price, date]
    consumer.append(c)

# checking whether the middle has earned profit or not
def max_profit(producer, consumer):
    max_earning = 0
    for p in producer:
        for c in consumer:
            if p[0] < c[0] and p[1] < c[1]:
                earning = (c[0] - p[0]) * (c[1] - p[1])
                if earning > max_earning:
                    max_earning = earning
    return max_earning

print("profits earned by middleman is:", max_profit(producer, consumer))
