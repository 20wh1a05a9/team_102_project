print("enter the number of producer and consumer companies:")
m,n = map(int,input().split())
producer = []
for i in range(m):
    print("enter the price and date of producer company:")
    price,date = map(int,input().split())
    p = [price,date]
    producer.append(p)
consumer = []
for i in range(n):
    print("enter the price and date of consumer company:")
    price,date = map(int,input().split())
    c = [price,date]
    consumer.append(c)
def max_profit(m,n):
    profit = 0
    earning = 0
    for p in producer:
        for c in consumer:
            if p[0] < c[0] and p[1] < c[1]:
                earning = (c[0] - p[0]) * (c[1] - p[1])
            if earning > profit:
                profit = earning
    return profit
print("profits earned by middleman is:" , max_profit(m,n))
                        
               
    

