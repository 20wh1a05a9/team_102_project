# Team_102_project
**TEAM MEMBERS:**

- Akanksha B Vennu - 20wh1a05a9 
- Kalekere  Sunidhi - 20wh1a1284
- Shaik Ramjanbee - 20wh1a05h2 
- Manda Rajeshwari - 21wh5a0505 
- Paakurthi Shinee- 20wh1a05e0 
- B V Meghamala- 20wh1a0462 

**PROBLEM STATEMENT:**
In this problem you will be solving one of the most profoyund challanges of humans across the world since the beginning of time - how to make lots of money.

You are a middleman in the widget market. Your job is to buy widgets from the producer company and sell them to widget consumer company.Each widget consumer companyhas an open request for one widget per day,untill some end date,and a price at which it is willing to buy the widgets.On the other hand, each widget producer company has a start date at which it can start delevering the widgets and a price at which it will deliver each widget.

Due to fair competition laws, you can sign a contract with only one producer company and only one consumer company.You will buy widgets from the producer company,one per day,startingon the day it can start delivering and ending on the date specified by the consumer company.On each of those days you earn the difference between the producer's selling price and consumer's buying price.

Your goal is to choose the consumer companyand the  producer company that will maximize your profits.




